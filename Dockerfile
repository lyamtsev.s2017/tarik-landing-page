FROM node:14.2.0 AS nodejs
WORKDIR /build/
ENV PATH /build/node_modules/.bin:/build:$PATH

COPY package.json .
RUN npm install
COPY . .
RUN cp src/server_config.js src/config.js
RUN npm run build


FROM nginx AS prod

COPY --from=nodejs /build/dist /app
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
