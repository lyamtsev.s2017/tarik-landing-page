export default api => ({
  create: data => api.post('user/create', data),
  verify: token => api.post('user/verify', {token})
})
